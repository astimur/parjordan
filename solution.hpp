#ifndef solution_hpp
#define solution_hpp

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <pthread.h>
#include <sched.h>

#define eps 1e-20

typedef struct
{
    int n;
    double *A;
    double *b;
    double *x;
    int *index;
    int *errorflag;
    int threadNum;
    int threadCount;
    int flag;
} ARGS;

void *Function(void *Arg);

int solve(int n, double* A, double* b, double* x, int* index, int* flag, int threadNum, int threadCount);

void Residual(int n, int m, double* A, double* b, double* x);

void Error_func(int n, double* x);

void synchronize(int total_threads);

double get_full_time();

#endif /* solution_hpp */
