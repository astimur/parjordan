#include "matrix.hpp"
#include "solution.hpp"

using namespace std;

int main()
{
    FILE* in = NULL;
    int i,n,m,input_method,s;
    double* A;
    double* x;
    double* b;
    int* index;
    double t;
    int threadCount, flag;
    pthread_t *threads;
    ARGS *Args;
    
    printf("\nChoose your input method: \n       1.From file \n       2.Random matrix\n-> ");
    scanf("%d",&input_method);
    printf("\n");
    
    if (input_method == 1)
    {
        char* file_name;
        file_name = new char[20];
        printf("Enter file name: ");
        if(!scanf("%s", file_name))
        {
            printf("Error reading\n");
            return -1;
        };
        printf("\n");
        in = fopen(file_name, "r");
        delete []file_name;
        if (!in)
        {
            printf("Error opening file\n\n");
            return -1;
        }
        
        if (!fscanf(in, "%d", &n))
        {
            printf("Error reading from file\n\n");
            fclose(in);
            return -1;
        }
    }
    else if (input_method == 2)
    {
        printf("Enter matrix size (n): ");
        if(!scanf("%d",&n))
            return -1;
        printf("\n");
    }
    else
    {
        printf("Incorrect method\n\n");
        return -1;
    }
    
    if (n<1)
    {
        printf("Incorrect matrix size\n\n");
        if (input_method == 1)
            fclose(in);
        return -1;
    }
    
    printf("Enter thread count: ");
    s =scanf("%d", &threadCount);
    
    if (!s || threadCount <= 0)
    {
        printf("Incorrect thread count\n\n");
        if (input_method == 1)
            fclose(in);
        return -1;
    }
    
    A = new double[n*n];
    x = new double[n];
    b = new double[n];
    index = new int[n]; // Numbers of variables (will be used while swapping columns)
    threads = new pthread_t[threadCount];
    Args = new ARGS[threadCount];
    
    s = input(A,b,input_method,n,in);
    
    if (s == -1)
    {
        printf("Error reading from file\n\n");
        fclose(in);
        delete []A;
        delete []x;
        delete []b;
        delete []index;
        delete []threads;
        delete []Args;
        
        return -1;
    }
    
    flag = 1;
    
    for (i = 0; i < threadCount; i++)
    {
        Args[i].n = n;
        Args[i].A = A;
        Args[i].b = b;
        Args[i].x = x;
        Args[i].index = index;
        Args[i].errorflag = &flag;
        Args[i].threadNum = i;
        Args[i].threadCount = threadCount;
    }
    
    t = get_full_time();
    
    for (i = 0; i < threadCount; i++)
    {
        if (pthread_create(threads + i, 0, Function, Args + i))
        {
            printf("Error creating thread %d!\n", i);
            if (input_method == 1)
                fclose(in);
            delete []A;
            delete []x;
            delete []b;
            delete []index;
            delete []threads;
            delete []Args;
            
            return -1;
        }
    }
    
    for (i = 0; i < threadCount; i++)
        if (pthread_join(threads[i], 0))
        {
            printf("Can't wait thread №%d!\n", i);
            if (input_method == 1)
                fclose(in);
            delete []A;
            delete []x;
            delete []b;
            delete []index;
            delete []threads;
            delete []Args;
            
            return -1;
        }
    
    t = get_full_time() - t;
    
    for (i = 0; i < threadCount; i++)
        if (Args[i].flag == -1)
        {
            printf("Degenerate matrix. Can't find the solution.\n\n");
            if (input_method == 1)
                fclose(in);
            delete []A;
            delete []x;
            delete []b;
            delete []index;
            delete []threads;
            delete []Args;
            
            return -1;
        }
    
    printf("Completed!\n\nMaximum output symbols: ");
    if (!scanf("%d",&m))
    {
        printf("Incorrect value.\n\n");
        if (input_method == 1)
            fclose(in);
        delete []A;
        delete []x;
        delete []b;
        delete []index;
        delete []threads;
        delete []Args;
        
        return -1;
    }
    
    Display(n,m,x);
    
    if (input_method == 1)
        fseek(in,1,SEEK_SET);
    s = input(A,b,input_method,n,in);
    
    Residual(n,m,A,b,x);
    
    if (input_method == 2)
        Error_func(n,x);
    
    printf("\nTime: %.5f sec.\n\n", double(t));
    
    if (input_method == 1)
        fclose(in);
    delete []A;
    delete []x;
    delete []b;
    delete []index;
    delete []threads;
    delete []Args;
    
    return 0;
}
