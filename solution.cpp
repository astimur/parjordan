#include "solution.hpp"

int flag = 1;

void *Function(void *Arg)
{
    ARGS *arg = (ARGS*)Arg;
    arg->flag = solve(arg->n, arg->A, arg->b, arg->x, arg->index, arg->errorflag, arg->threadNum, arg->threadCount);
    return NULL;
}

int solve(int n, double* A, double* b, double* x, int* index, int* flag, int threadNum, int threadCount)
{
    int i, j, k, maxNum, rows;
    double maxElem, tmp;
    
    int rowStart, rowEnd;
    
    if (threadNum == 0)
    {
        for (i=0; i<n; i++)
            index[i] = i;
    }

    for (i=0; i<n; i++)
    {
        
        synchronize(threadCount);
        
        if(threadNum == 0) // Search for max in line
        {
            maxElem = A[i*n+i];
            maxNum = i;
            for (j=i+1; j<n; j++)
                if (fabs(A[i*n+j]) > fabs(maxElem))
                {
                    maxElem = A[i*n+j];
                    maxNum = j;
                }
            if (fabs(maxElem) < eps) // Zero line (i.e. detA = 0)
                *flag = 0;
            if (maxNum != i && *flag) // Swap columns (i <-> maxNum)
            {
                for (j=0; j<n; j++)
                {
                    maxElem = A[j*n+maxNum];
                    A[j*n+maxNum] = A[j*n+i];
                    A[j*n+i] = maxElem;
                }
                k = index[maxNum]; // Swap numbers of variables
                index[maxNum] = index[i];
                index[i] = k;
            }
        }
        
        synchronize(threadCount);

        if (!*flag)
            return -1;
        
        if (n % threadCount == 0)
        {
            rows = n/threadCount;
            rowStart = rows*(threadNum);
            rowEnd = rows*(threadNum+1);
        }
        else
        {
            rows = int(n/(threadCount));
            rowStart = rows*(threadNum);
            if (threadNum == threadCount-1)
                rowEnd = n;
            else
                rowEnd = rows*(threadNum+1);
        }
        
        for(j=rowStart; j<rowEnd; j++)
            if (j != i)
            {
                tmp = A[j*n+i]/A[i*n+i];
                for(k=i; k<n;k++)
                    A[j*n+k] -= tmp*A[i*n+k];
                b[j] -= tmp*b[i];
            }
    }
    
    synchronize(threadCount);
    
    if (threadNum == 0)
        for (i=0; i<n; i++)
            x[index[i]] = b[i]/A[i*n+i];
    
    printf("I'm thread %d, my rows are %d %d \n", threadNum, rowStart, rowEnd);

    return 0;
}

void Residual(int n, int m, double* A, double* b, double* x)
{
    int i,j;
    double Res = 0;
    double tmp;
    printf("Residual: \n");
    for (i=0; i<n; i++)
    {
        tmp = 0;
        for (j=0; j<n; j++)
            tmp += A[i*n+j]*x[j];
        tmp -= b[i];
        if (i < m) printf("%f ",tmp);
        Res += tmp*tmp;
    }
    
    printf("\n\n");
    printf("Norm of the residual: %f\n",sqrt(Res));
    return;
}

void Error_func(int n, double* x)
{
    double er = 0;
    for (int i=0; i<n; i++)
        if (i % 2)
            er += (x[i])*(x[i]);
        else
            er += (x[i]-1)*(x[i]-1);
    
    printf("\n");
    printf("Norm of the error: %f\n",sqrt(er));
    return;
}

void synchronize(int total_threads)
{
    static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    static pthread_cond_t condvar_in = PTHREAD_COND_INITIALIZER;
    static pthread_cond_t condvar_out = PTHREAD_COND_INITIALIZER;
    static int threads_in = 0;
    static int threads_out = 0;
    
    pthread_mutex_lock(&mutex);
    
    threads_in++;
    if (threads_in >= total_threads)
    {
        threads_out = 0;
        pthread_cond_broadcast(&condvar_in);
    }
    else
        while (threads_in < total_threads)
            pthread_cond_wait(&condvar_in,&mutex);
    
    threads_out++;
    if (threads_out >= total_threads)
    {
        threads_in = 0;
        pthread_cond_broadcast(&condvar_out);
    } else
        while (threads_out < total_threads)
            pthread_cond_wait(&condvar_out,&mutex);
    
    pthread_mutex_unlock(&mutex);
}

double get_full_time()
{
    struct timeval t;
    gettimeofday(&t, NULL);
    return (double)t.tv_sec + (double)t.tv_usec / 1000000.;
}
