#ifndef matrix_hpp
#define matrix_hpp

#include <stdio.h>

int input(double* A, double* b, int input_method, int n, FILE* in);

void Display(int n, int m, double* x);

double f(int i, int j, int n);

#endif /* matrix_hpp */
